package ru.t1.panasyuk.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.api.model.IWBS;
import ru.t1.panasyuk.tm.constant.DBConst;
import ru.t1.panasyuk.tm.enumerated.Status;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = DBConst.TABLE_TASK)
public final class TaskDTO extends AbstractUserOwnedModelDTO implements IWBS {

    private static final long serialVersionUID = 1;

    @NotNull
    @Column(name = DBConst.COLUMN_NAME)
    private String name = "";

    @Nullable
    @Column(name = DBConst.COLUMN_DESCRIPTION)
    private String description = "";

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = DBConst.COLUMN_STATUS, length = 30)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = DBConst.COLUMN_PROJECT_ID, length = 36)
    private String projectId;

    @NotNull
    @Override
    public String toString() {
        return String.format(
                "[Name=%s, Description=%s, Created=%tF %tT, Status=%s]",
                name, description, created, created, Status.toName(status)
        );
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public boolean equals(@NotNull final Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof TaskDTO)) return false;
        @NotNull final TaskDTO task = (TaskDTO) obj;
        return task.getId().equals(this.getId());
    }

}