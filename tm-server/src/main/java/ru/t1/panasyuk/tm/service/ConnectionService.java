package ru.t1.panasyuk.tm.service;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.api.service.IConnectionService;
import ru.t1.panasyuk.tm.api.service.IPropertyService;
import ru.t1.panasyuk.tm.dto.model.ProjectDTO;
import ru.t1.panasyuk.tm.dto.model.SessionDTO;
import ru.t1.panasyuk.tm.dto.model.TaskDTO;
import ru.t1.panasyuk.tm.dto.model.UserDTO;
import ru.t1.panasyuk.tm.model.Project;
import ru.t1.panasyuk.tm.model.Session;
import ru.t1.panasyuk.tm.model.Task;
import ru.t1.panasyuk.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

public final class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public ConnectionService(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
        this.entityManagerFactory = getEntityManagerFactory();
    }

    @NotNull
    @Override
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    @NotNull
    private EntityManagerFactory getEntityManagerFactory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        @NotNull final String username = propertyService.getDBUser();
        @NotNull final String password = propertyService.getDBPassword();
        @NotNull final String url = propertyService.getDBUrl();
        @NotNull final String driver = propertyService.getDBDriver();
        @NotNull final String dialect = propertyService.getDBDialect();
        @NotNull final String hbm2DdlAuto = propertyService.getDBHbm2DdlAuto();
        @NotNull final Boolean showSql = propertyService.getDBLoggingEnabled();
        @NotNull final String lazyLoadNoTransEnabed = propertyService.getDBLazyLoadNoTransEnabled();
        settings.put(org.hibernate.cfg.Environment.DRIVER, driver);
        settings.put(org.hibernate.cfg.Environment.URL, url);
        settings.put(org.hibernate.cfg.Environment.USER, username);
        settings.put(org.hibernate.cfg.Environment.PASS, password);
        settings.put(org.hibernate.cfg.Environment.DIALECT, dialect);
        settings.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, hbm2DdlAuto);
        settings.put(org.hibernate.cfg.Environment.SHOW_SQL, showSql.toString());
        settings.put(Environment.ENABLE_LAZY_LOAD_NO_TRANS, lazyLoadNoTransEnabed);
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry standardServiceRegistry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(standardServiceRegistry);
        sources.addAnnotatedClass(ProjectDTO.class);
        sources.addAnnotatedClass(SessionDTO.class);
        sources.addAnnotatedClass(TaskDTO.class);
        sources.addAnnotatedClass(UserDTO.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(Session.class);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(User.class);
        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

}