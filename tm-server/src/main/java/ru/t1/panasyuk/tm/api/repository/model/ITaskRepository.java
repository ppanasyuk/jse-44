package ru.t1.panasyuk.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @Nullable
    List<Task> findAllByProjectId(@NotNull String userId, @Nullable String projectId);

    void removeAllByProjectId(@NotNull String userId, @Nullable String projectId);

}