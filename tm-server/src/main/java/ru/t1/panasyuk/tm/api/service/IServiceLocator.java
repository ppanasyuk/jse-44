package ru.t1.panasyuk.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.api.service.dto.*;
import ru.t1.panasyuk.tm.api.service.model.*;

public interface IServiceLocator {

    @NotNull
    IAuthDtoService getAuthDtoService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IProjectDtoService getProjectDtoService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IProjectTaskDtoService getProjectTaskDtoService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ITaskDtoService getTaskDtoService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IUserDtoService getUserDtoService();

    @NotNull
    IUserService getUserService();

}