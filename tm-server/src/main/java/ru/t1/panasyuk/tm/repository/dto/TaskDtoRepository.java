package ru.t1.panasyuk.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.panasyuk.tm.constant.FieldConst;
import ru.t1.panasyuk.tm.dto.model.TaskDTO;

import javax.persistence.EntityManager;
import java.util.List;

public final class TaskDtoRepository extends AbstractUserOwnedDtoRepository<TaskDTO> implements ITaskDtoRepository {

    public TaskDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected @NotNull Class<TaskDTO> getEntityClass() {
        return TaskDTO.class;
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        @NotNull final String jpql = String.format(
                "FROM %s m WHERE m.%s = :%s AND m.%s = :%s ORDER BY m.%s",
                getEntityClass().getSimpleName(),
                FieldConst.FIELD_USER_ID,
                FieldConst.FIELD_USER_ID,
                FieldConst.FIELD_PROJECT_ID,
                FieldConst.FIELD_PROJECT_ID,
                FieldConst.FIELD_CREATED
        );
        return entityManager
                .createQuery(jpql, getEntityClass())
                .setParameter(FieldConst.FIELD_USER_ID, userId)
                .setParameter(FieldConst.FIELD_PROJECT_ID, projectId)
                .getResultList();
    }

    @Override
    public void removeAllByProjectId(@NotNull String userId, @Nullable String projectId) {
        @NotNull final String jpql = String.format(
                "DELETE FROM %s m WHERE m.%s = :%s AND m.%s = :%s",
                getEntityClass().getSimpleName(),
                FieldConst.FIELD_USER_ID,
                FieldConst.FIELD_USER_ID,
                FieldConst.FIELD_PROJECT_ID,
                FieldConst.FIELD_PROJECT_ID
        );
        entityManager
                .createQuery(jpql)
                .setParameter(FieldConst.FIELD_USER_ID, userId)
                .setParameter(FieldConst.FIELD_PROJECT_ID, projectId)
                .executeUpdate();
    }

}