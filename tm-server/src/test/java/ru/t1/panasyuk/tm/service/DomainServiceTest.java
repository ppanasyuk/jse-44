package ru.t1.panasyuk.tm.service;

import io.qameta.allure.junit4.DisplayName;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.panasyuk.tm.api.service.*;
import ru.t1.panasyuk.tm.api.service.dto.*;
import ru.t1.panasyuk.tm.api.service.model.*;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.dto.model.ProjectDTO;
import ru.t1.panasyuk.tm.dto.model.TaskDTO;
import ru.t1.panasyuk.tm.dto.model.UserDTO;
import ru.t1.panasyuk.tm.service.dto.*;
import ru.t1.panasyuk.tm.service.model.*;

import java.util.List;

@DisplayName("Тестирование сервиса DomainService")
public class DomainServiceTest {

    @NotNull
    private IServiceLocator serviceLocator;

    @NotNull
    private UserDTO test;

    @NotNull
    private UserDTO admin;

    @NotNull
    private static IConnectionService connectionService;

    @BeforeClass
    public static void initConnection() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @Before
    public void initService() throws Exception {
        serviceLocator = new IServiceLocator() {

            @Getter
            @NotNull
            final IPropertyService propertyService = new PropertyService();

            @Getter
            @NotNull
            final IProjectDtoService projectDtoService = new ProjectDtoService(connectionService);

            @Getter
            @NotNull
            final IProjectService projectService = new ProjectService(connectionService);

            @Getter
            @NotNull
            final ITaskDtoService taskDtoService = new TaskDtoService(connectionService);

            @Getter
            @NotNull
            final ITaskService taskService = new TaskService(connectionService);

            @Getter
            @NotNull
            final IProjectTaskDtoService projectTaskDtoService = new ProjectTaskDtoService(connectionService);

            @Getter
            @NotNull
            final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);

            @Getter
            @NotNull
            final IUserDtoService userDtoService = new UserDtoService(propertyService, connectionService, projectTaskDtoService);

            @Getter
            @NotNull
            final IUserService userService = new UserService(propertyService, connectionService);

            @Getter
            @NotNull
            final IDomainService domainService = new DomainService(this);

            @Getter
            @NotNull
            final ISessionDtoService sessionDtoService = new SessionDtoService(connectionService);

            @Getter
            @NotNull
            final ISessionService sessionService = new SessionService(connectionService);

            @Getter
            @NotNull
            final IAuthDtoService authDtoService = new AuthDtoService(propertyService, userDtoService, sessionDtoService);

            @Getter
            @NotNull
            final IAuthService authService = new AuthService(propertyService, userService, sessionService);

            @Getter
            @NotNull
            final ILoggerService loggerService = new LoggerService(propertyService);

        };

        test = serviceLocator.getUserDtoService().create("TEST", "TEST", "TEST@TEST.ru");
        admin = serviceLocator.getUserDtoService().create("ADMIN", "ADMIN", "ADMIN@TEST.ru", Role.ADMIN);
        @NotNull final ProjectDTO project1 = serviceLocator.getProjectDtoService().create(test.getId(), "Project 1", "Project for TEST");
        @NotNull final ProjectDTO project2 = serviceLocator.getProjectDtoService().create(admin.getId(), "Project 2", "Project for ADMIN");
        @NotNull final ProjectDTO project3 = serviceLocator.getProjectDtoService().create(test.getId(), "Project 3", "Project for TEST 2");
        @NotNull final TaskDTO task1 = serviceLocator.getTaskDtoService().create(test.getId(), "Task 1", "Task for project 1");
        task1.setProjectId(project1.getId());
        serviceLocator.getTaskDtoService().update(task1);
        @NotNull final TaskDTO task2 = serviceLocator.getTaskDtoService().create(test.getId(), "Task 2", "Task for project 1");
        task2.setProjectId(project1.getId());
        serviceLocator.getTaskDtoService().update(task2);
        @NotNull final TaskDTO task3 = serviceLocator.getTaskDtoService().create(admin.getId(), "Task 3", "Task for project 2");
        task3.setProjectId(project2.getId());
        serviceLocator.getTaskDtoService().update(task3);
        @NotNull final TaskDTO task4 = serviceLocator.getTaskDtoService().create(admin.getId(), "Task 4", "Task for project 2");
        task4.setProjectId(project2.getId());
        serviceLocator.getTaskDtoService().update(task4);
    }

    @After
    public void afterTest() throws Exception {
        serviceLocator.getTaskDtoService().clear(test.getId());
        serviceLocator.getTaskDtoService().clear(admin.getId());
        serviceLocator.getProjectDtoService().clear(test.getId());
        serviceLocator.getProjectDtoService().clear(admin.getId());
        serviceLocator.getUserDtoService().remove(admin);
        serviceLocator.getUserDtoService().remove(test);
    }

    @Test
    @DisplayName("Сохранение и загрузка бэкапа")
    public void dataBackupTest() throws Exception {
        @Nullable final List<ProjectDTO> projects = serviceLocator.getProjectDtoService().findAll(test.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<TaskDTO> tasks = serviceLocator.getTaskDtoService().findAll(test.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @Nullable final List<UserDTO> users = serviceLocator.getUserDtoService().findAll();
        Assert.assertNotNull(users);
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataBackup();
        serviceLocator.getTaskDtoService().clear();
        serviceLocator.getProjectDtoService().clear();
        serviceLocator.getUserDtoService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectDtoService().getSize(test.getId()));
        Assert.assertEquals(0, serviceLocator.getTaskDtoService().getSize(test.getId()));
        Assert.assertEquals(0, serviceLocator.getUserDtoService().getSize());
        serviceLocator.getDomainService().loadDataBackup();
        Assert.assertEquals(projects, serviceLocator.getProjectDtoService().findAll(test.getId()));
        Assert.assertEquals(tasks, serviceLocator.getTaskDtoService().findAll(test.getId()));
        Assert.assertEquals(users, serviceLocator.getUserDtoService().findAll());
    }

    @Test
    @DisplayName("Сохранение и загрузка данных в фомате Base64")
    public void dataBase64Test() throws Exception {
        @Nullable final List<ProjectDTO> projects = serviceLocator.getProjectDtoService().findAll(test.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<TaskDTO> tasks = serviceLocator.getTaskDtoService().findAll(test.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @Nullable final List<UserDTO> users = serviceLocator.getUserDtoService().findAll();
        Assert.assertNotNull(users);
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataBase64();
        serviceLocator.getTaskDtoService().clear();
        serviceLocator.getProjectDtoService().clear();
        serviceLocator.getUserDtoService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectDtoService().getSize(test.getId()));
        Assert.assertEquals(0, serviceLocator.getTaskDtoService().getSize(test.getId()));
        Assert.assertEquals(0, serviceLocator.getUserDtoService().getSize());
        serviceLocator.getDomainService().loadDataBase64();
        Assert.assertEquals(projects, serviceLocator.getProjectDtoService().findAll(test.getId()));
        Assert.assertEquals(tasks, serviceLocator.getTaskDtoService().findAll(test.getId()));
        Assert.assertEquals(users, serviceLocator.getUserDtoService().findAll());
    }

    @Test
    @DisplayName("Сохранение и загрузка данных в фомате Binary")
    public void dataBinaryTest() throws Exception {
        @Nullable final List<ProjectDTO> projects = serviceLocator.getProjectDtoService().findAll(test.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<TaskDTO> tasks = serviceLocator.getTaskDtoService().findAll(test.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @Nullable final List<UserDTO> users = serviceLocator.getUserDtoService().findAll();
        Assert.assertNotNull(users);
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataBinary();
        serviceLocator.getTaskDtoService().clear();
        serviceLocator.getProjectDtoService().clear();
        serviceLocator.getUserDtoService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectDtoService().getSize(test.getId()));
        Assert.assertEquals(0, serviceLocator.getTaskDtoService().getSize(test.getId()));
        Assert.assertEquals(0, serviceLocator.getUserDtoService().getSize());
        serviceLocator.getDomainService().loadDataBinary();
        Assert.assertEquals(projects, serviceLocator.getProjectDtoService().findAll(test.getId()));
        Assert.assertEquals(tasks, serviceLocator.getTaskDtoService().findAll(test.getId()));
        Assert.assertEquals(users, serviceLocator.getUserDtoService().findAll());
    }

    @Test
    @DisplayName("Сохранение и загрузка данных в фомате Json используя FasterXML")
    public void dataJsonFasterXMLTest() throws Exception {
        @Nullable final List<ProjectDTO> projects = serviceLocator.getProjectDtoService().findAll(test.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<TaskDTO> tasks = serviceLocator.getTaskDtoService().findAll(test.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @Nullable final List<UserDTO> users = serviceLocator.getUserDtoService().findAll();
        Assert.assertNotNull(users);
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataJsonFasterXML();
        serviceLocator.getTaskDtoService().clear();
        serviceLocator.getProjectDtoService().clear();
        serviceLocator.getUserDtoService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectDtoService().getSize(test.getId()));
        Assert.assertEquals(0, serviceLocator.getTaskDtoService().getSize(test.getId()));
        Assert.assertEquals(0, serviceLocator.getUserDtoService().getSize());
        serviceLocator.getDomainService().loadDataJsonFasterXML();
        Assert.assertEquals(projects, serviceLocator.getProjectDtoService().findAll(test.getId()));
        Assert.assertEquals(tasks, serviceLocator.getTaskDtoService().findAll(test.getId()));
        Assert.assertEquals(users, serviceLocator.getUserDtoService().findAll());
    }

    @Test
    @DisplayName("Сохранение и загрузка данных в фомате Json используя JaxB")
    public void dataJsonJaxBTest() throws Exception {
        @Nullable final List<ProjectDTO> projects = serviceLocator.getProjectDtoService().findAll(test.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<TaskDTO> tasks = serviceLocator.getTaskDtoService().findAll(test.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @Nullable final List<UserDTO> users = serviceLocator.getUserDtoService().findAll();
        Assert.assertNotNull(users);
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataJsonJaxB();
        serviceLocator.getTaskDtoService().clear();
        serviceLocator.getProjectDtoService().clear();
        serviceLocator.getUserDtoService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectDtoService().getSize(test.getId()));
        Assert.assertEquals(0, serviceLocator.getTaskDtoService().getSize(test.getId()));
        Assert.assertEquals(0, serviceLocator.getUserDtoService().getSize());
        serviceLocator.getDomainService().loadDataJsonJaxB();
        Assert.assertEquals(projects, serviceLocator.getProjectDtoService().findAll(test.getId()));
        Assert.assertEquals(tasks, serviceLocator.getTaskDtoService().findAll(test.getId()));
        Assert.assertEquals(users, serviceLocator.getUserDtoService().findAll());
    }

    @Test
    @DisplayName("Сохранение и загрузка данных в фомате XML используя FasterXML")
    public void dataXMLFasterXMLTest() throws Exception {
        @Nullable final List<ProjectDTO> projects = serviceLocator.getProjectDtoService().findAll(test.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<TaskDTO> tasks = serviceLocator.getTaskDtoService().findAll(test.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @Nullable final List<UserDTO> users = serviceLocator.getUserDtoService().findAll();
        Assert.assertNotNull(users);
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataXMLFasterXML();
        serviceLocator.getTaskDtoService().clear();
        serviceLocator.getProjectDtoService().clear();
        serviceLocator.getUserDtoService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectDtoService().getSize(test.getId()));
        Assert.assertEquals(0, serviceLocator.getTaskDtoService().getSize(test.getId()));
        Assert.assertEquals(0, serviceLocator.getUserDtoService().getSize());
        serviceLocator.getDomainService().loadDataXMLFasterXML();
        Assert.assertEquals(projects, serviceLocator.getProjectDtoService().findAll(test.getId()));
        Assert.assertEquals(tasks, serviceLocator.getTaskDtoService().findAll(test.getId()));
        Assert.assertEquals(users, serviceLocator.getUserDtoService().findAll());
    }

    @Test
    @DisplayName("Сохранение и загрузка данных в фомате XML используя JaxB")
    public void dataXMLJaxBTest() throws Exception {
        @Nullable final List<ProjectDTO> projects = serviceLocator.getProjectDtoService().findAll(test.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<TaskDTO> tasks = serviceLocator.getTaskDtoService().findAll(test.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @Nullable final List<UserDTO> users = serviceLocator.getUserDtoService().findAll();
        Assert.assertNotNull(users);
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataXMLJaxB();
        serviceLocator.getTaskDtoService().clear();
        serviceLocator.getProjectDtoService().clear();
        serviceLocator.getUserDtoService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectDtoService().getSize(test.getId()));
        Assert.assertEquals(0, serviceLocator.getTaskDtoService().getSize(test.getId()));
        Assert.assertEquals(0, serviceLocator.getUserDtoService().getSize());
        serviceLocator.getDomainService().loadDataXMLJaxB();
        Assert.assertEquals(projects, serviceLocator.getProjectDtoService().findAll(test.getId()));
        Assert.assertEquals(tasks, serviceLocator.getTaskDtoService().findAll(test.getId()));
        Assert.assertEquals(users, serviceLocator.getUserDtoService().findAll());
    }

    @Test
    @DisplayName("Сохранение и загрузка данных в фомате Yaml используя FasterXML")
    public void dataYamlFasterXMLTest() throws Exception {
        @Nullable final List<ProjectDTO> projects = serviceLocator.getProjectDtoService().findAll(test.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<TaskDTO> tasks = serviceLocator.getTaskDtoService().findAll(test.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @Nullable final List<UserDTO> users = serviceLocator.getUserDtoService().findAll();
        Assert.assertNotNull(users);
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataYamlFasterXML();
        serviceLocator.getTaskDtoService().clear();
        serviceLocator.getProjectDtoService().clear();
        serviceLocator.getUserDtoService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectDtoService().getSize(test.getId()));
        Assert.assertEquals(0, serviceLocator.getTaskDtoService().getSize(test.getId()));
        Assert.assertEquals(0, serviceLocator.getUserDtoService().getSize());
        serviceLocator.getDomainService().loadDataYamlFasterXML();
        Assert.assertEquals(projects, serviceLocator.getProjectDtoService().findAll(test.getId()));
        Assert.assertEquals(tasks, serviceLocator.getTaskDtoService().findAll(test.getId()));
        Assert.assertEquals(users, serviceLocator.getUserDtoService().findAll());
    }

}